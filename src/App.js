import React, { useRef, useState, useEffect } from "react";
import Table from "./components/Table";
import {
  HighMarginReceipe,
  LowMarginReceipe,
  FluctuatingReceipe,
} from "./components/QuickBlock";
export default function App() {
  const [page, setPage] = useState(1);
  const prev = useRef(null);
  const next = useRef(null);
  // useEffect(() => {
  //   prev.current.disabled = "true";
  // }, []);
  useEffect(() => {
    if (page === 2) {
      prev.current.disabled = false;
      next.current.disabled = true;
    } else if (page === 1) {
      prev.current.disabled = true;
      next.current.disabled = false;
    }
  }, [page]);

  const changePage = (e) => {
    e.preventDefault();
    if (e.target.id === "prev") {
      setPage((page) => page - 1);
    } else {
      setPage((page) => page + 1);
    }
  };
  return (
    <div className="App">
      <div className="container">
        <HighMarginReceipe />
        <LowMarginReceipe />
        <FluctuatingReceipe />
      </div>
      <Table page={page} />
      <div className="button-div">
        <button className="btn" ref={prev} id="prev" onClick={changePage}>
          &lt;
        </button>
        <p style={{ display: "inline" }}>{page}</p>
        <button className="btn" ref={next} id="next" onClick={changePage}>
          &gt;
        </button>
      </div>
    </div>
  );
}
