import React, { useRef, useEffect } from "react";

export function PercentCircle(props) {
  const circl = useRef(null);

  const { percentage, fcolor } = props;
  useEffect(() => {
    const radius = circl.current.r.baseVal.value;
    console.log(circl.current.r.baseVal.value);
    const circumference = radius * 2 * Math.PI;
    setProgress(percentage, circumference);
  }, []);
  const setProgress = (percent, circumference) => {
    const offset = circumference - (percent / 100) * circumference;
    circl.current.style.strokeDasharray = `${circumference} ${circumference}`;
    circl.current.style.strokeDashoffset = offset;
  };
  return (
    <>
      <svg class="progress-ring" height="120" width="120">
        <circle
          ref={circl}
          class="progress-ring__circle"
          stroke={fcolor}
          stroke-width="4"
          fill="transparent"
          r="28"
          cx="30"
          cy="30"
        />
      </svg>
      <h5 className="percentage" style={{ color: fcolor }}>
        {percentage} %
      </h5>
    </>
  );
}

export function PercentArrow(props) {
  const { color, percentage } = props;

  return (
    <div className="arrow_details">
      <h5 className="percentage_arrow" style={{ color: color }}>
        {percentage} %
      </h5>

      {color === "green" ? (
        <img
          className="arrow"
          src="https://img.icons8.com/ios-filled/50/26e07f/long-arrow-up.png"
        />
      ) : (
        <img
          className="arrow"
          src="https://img.icons8.com/ios-filled/50/fa314a/long-arrow-down.png"
        />
      )}
    </div>
  );
}
