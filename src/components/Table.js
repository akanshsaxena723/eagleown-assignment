import React, { useEffect, useState, useRef } from "react";
import moment from "moment";
import axios from "axios";

export default function Table(props) {
  const { page } = props;
  console.log("page", page);
  const [receipes, setReceipes] = useState([]);
  const [receipesToShow, setReceipesToShow] = useState(receipes);
  const [isClicked, setIsClicked] = useState(false);
  const all = useRef(null);
  const is_incorrect = useRef(null);
  const is_disabled = useRef(null);
  const is_untagged = useRef(null);
  const [checkedArray, setCheckedArray] = useState([
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ]);
  const [selected, setSelected] = useState([]);
  const [isChecked, setIsChecked] = useState(false);

  useEffect(async () => {
    const getData = async () => {
      const response = await axios.get(
        `https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?format=json&page=${page}`
      );
      const data = await response.data;
      return data.results;
    };
    setReceipes(await getData());
  }, []);
  useEffect(async () => {
    const getData = async () => {
      const response = await axios.get(
        `https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/recipes/?format=json&page=${page}`
      );
      const data = await response.data;
      return data.results;
    };
    setReceipes(await getData());
  }, [page]);
  useEffect(() => {
    setReceipesToShow(receipes);
    all.current.style.background = "#d9d9d9";
  }, [receipes]);

  const handleClick = (e) => {
    //For select all click
    if (e.target.name === "column") {
      setIsChecked(e.target.checked);
      let arr = checkedArray;
      if (!e.target.checked) {
        for (let i = 0; i < arr.length; i++) {
          arr[i] = false;
        }
        setCheckedArray(arr);
        setSelected([]);
      } else {
        for (let i = 0; i < arr.length; i++) {
          arr[i] = true;
        }
        setCheckedArray(arr);
        setSelected(receipes);
      }
      console.log(selected);
    }
  };
  const handleRClick = (e) => {
    let index = parseInt(e.target.name);
    let arr = selected;
    let arr2 = checkedArray;
    arr2[index] = !arr2[index];
    setIsClicked((isClicked) => !isClicked);
    setCheckedArray(arr2);
    if (e.target.checked) {
      arr.push(receipes.find((receipe) => receipe.id == e.target.id));
      setSelected(arr);
      setCheckedArray(arr2);
    } else {
      var ind;
      for (let i = 0; i < selected.length; i++) {
        if (parseInt(selected[i].id) === parseInt(e.target.id)) {
          arr.splice(i, 1);
          break;
        }
      }
      setSelected(arr);
    }
    console.log(selected);
  };

  //To handle the data to show according to entity filter
  const switchView = (e) => {
    e.preventDefault();
    if (e.target.id === "all") {
      setReceipesToShow(receipes);
      all.current.style.background = "#d9d9d9";
      is_incorrect.current.style.background = "white";
      is_disabled.current.style.background = "white";
      is_untagged.current.style.background = "white";
    } else if (e.target.id === "is_incorrect") {
      let arr = receipes;
      setReceipesToShow(arr.filter((arr1) => arr1[e.target.id] === true));
      all.current.style.background = "white";
      is_incorrect.current.style.background = "#d9d9d9";
      is_disabled.current.style.background = "white";
      is_untagged.current.style.background = "white";
    } else if (e.target.id === "is_untagged") {
      let arr = receipes;
      setReceipesToShow(arr.filter((arr1) => arr1[e.target.id] === true));
      all.current.style.background = "white";
      is_incorrect.current.style.background = "white";
      is_disabled.current.style.background = "white";
      is_untagged.current.style.background = "#d9d9d9";
    } else if (e.target.id === "is_disabled") {
      let arr = receipes;
      setReceipesToShow(arr.filter((arr1) => arr1[e.target.id] === true));
      all.current.style.background = "white";
      is_incorrect.current.style.background = "white";
      is_disabled.current.style.background = "#d9d9d9";
      is_untagged.current.style.background = "white";
    }
  };
  return (
    <div>
      <nav className="nav-bar">
        <h3 className="entity" ref={all} id="all" onClick={switchView}>
          All Receipe(s)
        </h3>
        <h3
          className="entity"
          ref={is_incorrect}
          id="is_incorrect"
          onClick={switchView}
        >
          Incorrect
        </h3>
        <h3
          className="entity"
          ref={is_untagged}
          id="is_untagged"
          onClick={switchView}
        >
          Untagged
        </h3>
        <h3
          className="entity"
          ref={is_disabled}
          id="is_disabled"
          onClick={switchView}
        >
          Disabled
        </h3>
      </nav>
      <div className="table">
        <div className="tabel-container">
          <div className="column">
            <input
              type="checkbox"
              id="column"
              name="column"
              value="all"
              onChange={handleClick}
              checked={isChecked}
            />
            <h5 style={{ width: "250px" }}>Name</h5>
            <h5 style={{ width: "100px" }}>Last Updated</h5>
            <h5>COGS</h5>
            <h5>Cost Price()</h5>
            <h5>Sale Price</h5>
            <h5>Gross Margin</h5>
            <h5 style={{ width: "150px" }}>Tags/Actions</h5>
          </div>
          <div className="receipes">
            {receipesToShow.length > 0 ? (
              receipesToShow.map((receipe, index) => (
                <div className="receipe">
                  <input
                    type="checkbox"
                    id={receipe.id}
                    name={index}
                    value={checkedArray[index]}
                    onChange={handleRClick}
                    checked={checkedArray[index]}
                  />
                  <p style={{ width: "200px" }}>{receipe.name}</p>
                  <p style={{ width: "150px" }}>
                    {moment(
                      receipe.last_updated.date.split(" ")[0],
                      "YYYY-MM-DD"
                    ).format("MMM Do YY")}
                  </p>
                  <p>{receipe.cogs}</p>
                  <p>{receipe.cost_price.toFixed(2)}</p>
                  <p>{receipe.sale_price.toFixed(2)}</p>
                  <p>{receipe.gross_margin.toFixed(2)}</p>
                  <p
                    style={{
                      width: "150px",
                      padding: "5px 5px",
                      borderRadius: "0.3rem",
                      background: "blue",
                      color: "white",
                      textAlign: "center",
                    }}
                  >
                    Famous
                  </p>
                </div>
              ))
            ) : (
              <h3 style={{ textAlign: "center", color: "red" }}>
                No receipes found
              </h3>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
