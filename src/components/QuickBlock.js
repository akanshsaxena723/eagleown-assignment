import React, { useState, useEffect } from "react";
import { PercentArrow, PercentCircle } from "./PercentCircle";
import axios from "axios";
export function HighMarginReceipe() {
  const [highMarginReceipes, setHighMarginReceipes] = useState([]);

  useEffect(() => {
    getHighMargin();
  }, []);

  const getHighMargin = async () => {
    const response = await axios.get(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/?order=top"
    );
    const data = response.data;
    setHighMarginReceipes(data.results);
  };

  return (
    <div className="dish-container">
      <h3 className="heading">High Margin Receipes</h3>
      <div className="outer-container">
        {highMarginReceipes.length > 0 ? (
          highMarginReceipes.map((element) => (
            <div className="inner-container">
              <div>
                <h4>{element.name}</h4>
                <PercentCircle
                  percentage={element.margin.toFixed(0)}
                  fcolor={"green"}
                />
              </div>
            </div>
          ))
        ) : (
          <p>No data</p>
        )}
      </div>
    </div>
  );
}

export function LowMarginReceipe() {
  const [lowMarginReceipes, setLowMarginReceipes] = useState([]);

  useEffect(() => {
    getLowMargin();
  }, []);

  const getLowMargin = async () => {
    const response = await axios.get(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/margin-group/?order=bottom"
    );
    const data = response.data;
    setLowMarginReceipes(data.results);
  };

  return (
    <div className="dish-container">
      <h3 className="heading">Low Margin Receipes</h3>
      <div className="outer-container">
        {lowMarginReceipes.length > 0 ? (
          lowMarginReceipes.map((element) => (
            <div className="inner-container">
              <div>
                <h4>{element.name}</h4>
                <PercentCircle
                  percentage={element.margin.toFixed(0)}
                  fcolor={"red"}
                />
              </div>
            </div>
          ))
        ) : (
          <p>No data</p>
        )}
      </div>
    </div>
  );
}

export function FluctuatingReceipe() {
  const [fluctuatingReceipes, setFluctuatingReceipes] = useState([]);

  useEffect(() => {
    getFluctuating();
  }, []);

  const getFluctuating = async () => {
    const response = await axios.get(
      "https://beta.eagleowl.in/api/v1/mock/organization/18/outlet/18/recipe/fluctuation-group/?order=top"
    );
    const data = response.data;
    setFluctuatingReceipes(data.results);
  };

  return (
    <div className="dish-container">
      <h3 className="heading">Top Fluctuating Receipes</h3>
      <div className="outer-container">
        {fluctuatingReceipes.length > 0 ? (
          fluctuatingReceipes.map((element, index) => (
            <>
              {index === 1 ? (
                <div className="inner-container">
                  <div>
                    <h4>{element.name}</h4>
                    <PercentArrow
                      percentage={element.fluctuation.toFixed(0)}
                      color={"green"}
                    />
                  </div>
                </div>
              ) : (
                <div className="inner-container">
                  <div>
                    <h4>{element.name}</h4>
                    <PercentArrow
                      percentage={element.fluctuation.toFixed(0)}
                      color={"red"}
                    />
                  </div>
                </div>
              )}
            </>
          ))
        ) : (
          <p>No data</p>
        )}
      </div>
    </div>
  );
}
