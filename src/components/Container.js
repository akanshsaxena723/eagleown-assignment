import React, { useEffect, useRef } from "react";
import PercentCircle from "./PercentCircle";
export default function Container(props) {
  const reference = useRef(null);
  const { receipe, color } = props;
  // useEffect(() => {
  //   reference.current.style.background = "white";
  //   console.log(reference.current);
  // }, [receipe]);
  return (
    <div ref={reference} style={{ background: "red" }}>
      {receipe.map((element) => {
        <div>
          <h4>{element.name}</h4>
          <PercentCircle percentage={element.margin} color={color} />
        </div>;
      })}
    </div>
  );
}
